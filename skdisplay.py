import altair as alt
import pandas as pd
from pathlib import Path

## private variables
__datad = Path('skdata')
__saved = Path('html')
__fnames = sorted(__datad.rglob('*.csv'))

def load(fname):
    '''
    SKのデータ（csv）を読み込んでデータフレームに変換する

    Parameters
    ----------
    fname : str or path-like
        ファイル名

    Returns
    -------
    pandas.DataFrame
        データフレーム
    '''    
    names = ['cable', 'charge', 'time', 'x', 'y', 'z']
    data = pd.read_csv(fname, comment='#', names=names)
    print(f'Loaded! {fname}')
    return data

def crosssection(data, x, y, color, title, w=300, h=300):
    '''
    イベントディスプレイを作成

    Parameters
    ----------
    data : pandas.DataFrame
        データフレーム
    x : str
        X軸の列名
    y : str
        Y軸の列名
    color : str
        z軸の列名
    title : str
        グラフのタイトル
    w : int, optional
        グラフの横幅, by default 300
    h : int, optional
        グラフの高さ, by default 300

    Returns
    -------
    altair.Chart
        グラフ
    '''    
    tips = list(data.columns)
    chart = alt.Chart(data).mark_circle().encode(
        x=x,
        y=y,
        color=color,
    ).properties(
        title=title,
        width=w,
        height=h,
    )
    return chart

def display(fname, color):
    data = load(fname)

    ## データを以下のように整理する
    ## 1. xy1 : X-Y断面（Z > 0）
    ## 2. xy2 : X-Y断面（Z < 0） 
    ## 3. yz1 : Y-Z断面（X > 0）
    ## 4. yz2 : Y-Z断面（X < 0）
    ## 5. xz1 : X-Z断面（Y > 0）
    ## 6. xz2 : X-Z断面（Y < 0）
    qdata = data.query('z >= 0')    
    xy1 = crosssection(qdata, x='x', y='y', color=color, title='X-Y 断面（Z>0）')
    qdata = data.query('z < 0')    
    xy2 = crosssection(qdata, x='x', y='y', color=color, title='X-Y 断面（Z<0）')

    qdata = data.query('x >= 0')    
    yz1 = crosssection(qdata, x='y', y='z', color=color, title='Y-Z 断面（X>0）')
    qdata = data.query('x < 0')    
    yz2 = crosssection(qdata, x='y', y='z', color=color, title='Y-Z 断面（X<0）')
    
    qdata = data.query('y >= 0')        
    xz1 = crosssection(qdata, x='x', y='z', color=color, title='X-Z 断面（Y>0）')
    qdata = data.query('y < 0')
    xz2 = crosssection(qdata, x='x', y='z', color=color, title='X-Z 断面（Y<0）')

    chart = (xy1 | yz1 | xz1) & (xy2 | yz2 | xz2)

    html = __saved / Path(fname).with_suffix('.html').name
    chart.save(str(html))
    print(f'Saved {html}')
    return chart


if __name__ == '__main__':
    import argparse

    description = 'スーパーカミオカンデのイベントディスプレイを作ってみよう！'
    epilog = '''
    # データの準備：
    イベントディスプレイの作成に必要なデータは
    [東京大学宇宙線研究所　神岡宇宙素粒子研究施設のページ]
    (http://www-sk.icrr.u-tokyo.ac.jp/sk/detector/eventdisplay-data.html)
    からダウンロードしてください（このリポジトリにはいれていません）。
    そして、ダウンロードして展開したファイルのディレクトリを``skdata/``のフォルダにコピペ（もしくは移動）してください。
    これで準備は完了です！
    '''
    ap = argparse.ArgumentParser(description=description,
    epilog=epilog)

    ap.add_argument('fname', help='filename')
    ap.add_argument('-t', '--time', action='store_true', help='Make display with time info')    
    args = ap.parse_args()

    print(args)

    fname = Path(args.fname)

    color = 'charge'
    if args.time:
        color = 'time'

    display(fname=fname, color=color)
